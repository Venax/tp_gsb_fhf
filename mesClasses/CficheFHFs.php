<?php

require_once 'Cdao.php';

class CficheFHF
{

    public $id;
    public $idVisiteur;
    public $mois;
    public $libelle;
    public $date;
    public $montant;
}
class CficheFHFs
{
    public $ocollficheFHFById;
    public $ocollficheFHFByIdVisiteur;

    public function __construct()
    {
        try {

                $dao =  new Cdao();
                $query = 'SELECT id,idVisiteur,mois,libelle,date,montant from LigneFraisHorsForfait';
                $lesObjetsficheFHF =  $dao->getTabObjetFromSql($query,'CficheFHF');
                $this->$ocollficheFHFById = array();
                $this->$ocollficheFHFByIdVisiteur = array();
                foreach ($lesObjetsficheFHF as $oficheFHF) {                               
                   $this->$ocollficheFHFById[$oficheFHF->id] = $oficheFHF;
                   $this->$ocollficheFHFByIdVisiteur[$oficheFHF->idVisiteur] = $oficheFHF;
                }
            unset($dao); 
            }
            catch(PDOException $e) {
             $msg = 'ERREUR PDO dans ' . $e->getFile() . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
             die($msg);
            }
    }    
   function select_ficheFHF($idVisiteur) {  
        
        foreach ($this->ocollficheFHFByIdVisiteur as $oficheFHF) {
            if($oficheFHF->idVisiteur == $idVisiteur)
            {
                return $oficheFHF;
            }
        }
        return NULL;
    }
    function getficheFHFById($sid)
    {        
        return $this->ocollficheFHFById[$sid];
    }    
    function getficheFHFByIdVisiteur($idVisiteur)
    {        
        return $this->ocollficheFHFByIdVisiteur[$idVisiteur];
    }
    function getCollficheFHFByIdVisiteur()
    {
        return $this->ocollficheFHFByIdVisiteur;
    }

}


