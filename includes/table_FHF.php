<div class="container">
    <h2 class="titreSection">Récapitulatif des frais hors forfait du mois <span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span></h2>
    </br>       
    <div class="col-md-12">        
        <table class="table table-hover">
            <thead>
                <tr>
                    <th class="col-md-5">Libellé</th>
                    <th class="col-md-2">Date</th>
                    <th class="col-md-2">Montant</th>
                    <th class="col-md-1">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    require_once '../mesClasses/CficheFHFs.php';
                    require_once 'functions.php';
                        $lesficheFHF = new CficheFHFs();
                        $oficheFHF = $lesVisiteurs->select_ficheFHF($_SESSION["idVisiteur"]);
                        if(!empty($oficheFHF))
                        {   
                            foreach($oficheFHF as $elmtFHF){
                                echo '<tr>';
                                    echo '<td>'.prepareChaineHtml($elmtFHF['libelle']).'</td>';
                                    echo '<td>'.prepareChaineHtml($elmtFHF['date']).'</td>';
                                    echo '<td style="color: red;">'.prepareChaineHtml($elmtFHF['montant']).'</td>';                        
                                    echo '<td><input type="button" class="btn btn-danger btn-sm" value="Supprimer" data-toggle="modal" data-target="#ConfirmerSuppr"/></td>';
                                echo '</tr>';
                            }
                        }
                        else
                        {
                            echo "Aucune Fiche FHF";
                        }            
                ?>
            </tbody>
        </table>
    </div>
</div>
    <!-- PARTIE MODAL -->
<div id="ConfirmerSuppr" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Confirmation de suppression</h4>
            </div>
            <div class="modal-body">
                <center>
                    <p>Voulez-vous vraiment supprimer cette ligne ?</p>
                    <p>C'est irréversible</p>
                </center>
            </div>
            <div class="modal-footer">
            <div class="col-md-12">
                    <div class="col-md-4">
                        <button type="button" class="btn btn-success">Valider</button>
                    </div>
                    <div class="col-md-offset-2 col-md-4">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
